
function getCarData(data, carId) {

    const carData = [];

    if (!Array.isArray(data)) {
        return carData;
    }
    if (carId === null || carId===undefined) {
        return carData;
    }



    for (let index = 0; index < data.length; index++) {

        if (data[index].id === carId) {
            carData.push(data[index]);
            break;
        }

    }

    return carData;
}
module.exports = getCarData;