
function getDataOfBmwAudi(data) {

    let bmwAndAudiCar = [];

    if (data === null || data===undefined) {
        return bmwAndAudiCar;
    } 
    if (!Array.isArray(data)) {
        return bmwAndAudiCar;
    }

    for (let index = 0; index < data.length; index++) {

        if (data[index].car_make === 'BMW' || data[index].car_make === 'Audi') {

            bmwAndAudiCar.push(data[index]);

        }


    }
    return bmwAndAudiCar;

   

}
module.exports = getDataOfBmwAudi;