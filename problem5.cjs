
function olderCarDataByYear(data) {

    let olderDataOfCar=[];
    if (data === null || data===undefined) {
        return olderDataOfCar;
    } 
    if (!Array.isArray(data)) {
        return olderDataOfCar;
    }

    for (let index = 0; index < data.length; index++) {

            if(data[index]<2000)
            {
      olderDataOfCar.push(data[index]);
            }

    }
  return olderDataOfCar;
    
}
module.exports = olderCarDataByYear;