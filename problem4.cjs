
function getCarDataByYear(data) {

    let yearDataOfCar=[];

    if (data === null || data===undefined) {
      return  yearDataOfCar;
  } 
    if (!Array.isArray(data)) {
      return yearDataOfCar;
  }

    for (let index = 0; index < data.length; index++) {

      yearDataOfCar.push(data[index].car_year);

    }
   return yearDataOfCar;
}
module.exports = getCarDataByYear;