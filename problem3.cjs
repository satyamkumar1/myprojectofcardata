
function getCarDataInAlphabaticalOrder(data) {
    //data.sort((a,b)=>a.car_model-b.car_model);

    if (data === null || data===undefined) {
      return ;
  } 

    if (!Array.isArray(data)) {
      return;
  }
    data.sort(function (a, b) {
        if (a.car_model< b.car_model) {
          return -1;
        }
        if (a.car_model> b.car_model) {
          return 1;
        }
        return 0;
      });
    return data;
}
module.exports = getCarDataInAlphabaticalOrder;