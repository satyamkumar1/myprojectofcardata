function getLastCarData(data) {

    if (data === null || data===undefined) {
        return ;
    }    

    if (!Array.isArray(data)) {
        return;
    }
    let index = data.length - 1;

    console.log(`last car is a ${data[index].car_make} ${data[index].car_model}`);



}
module.exports = getLastCarData;